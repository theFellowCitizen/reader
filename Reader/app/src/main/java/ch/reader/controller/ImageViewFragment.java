package ch.reader.controller;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.net.Uri;
import android.net.wifi.aware.ParcelablePeerHandle;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.github.chrisbanes.photoview.OnSingleFlingListener;
import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;
import java.net.URI;
import java.util.Arrays;

import ch.reader.R;
import ch.reader.constant.CONSTANT;
import ch.reader.constant.Config;
import ch.reader.controller.handler.ChapterHandler;
import ch.reader.controller.handler.PageHandler;
import ch.reader.databinding.FragmentImageviewBinding;

public class ImageViewFragment extends Fragment {

    //Bindings and stuff
    private FragmentImageviewBinding binding;
    private ConstraintLayout layout;
    private ScrollView scrollViewForScrollNav;
    private LinearLayout linearLayoutForScrollNav;
    private PhotoView photoView;
    private ImageView imageView;
    private TextView textNumber;

    private Config config                   = Config.getInstance();
    private PageHandler pageHandler         = PageHandler.getInstance();
    private ChapterHandler chapterHandler   = ChapterHandler.getInstance();

    private File[] files;
    private final int next_page     = 1;
    private final int previous_page = -1;
    private final int default_page  = 0;
    private PhotoView[] imageList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding     = FragmentImageviewBinding.inflate(inflater, container, false);
        layout      = binding.pageLayout;
        photoView   = binding.photoView;
        textNumber  = binding.textNumber;

        scrollViewForScrollNav = new ScrollView(getContext());
        linearLayoutForScrollNav = new LinearLayout(getContext());

        textNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                callDialog();
            }
        });

        photoView.setOnSingleFlingListener(new OnSingleFlingListener() {
            private static final int SWIPE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX)
                                > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                            result = true;
                        }
                    }
                    else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY)
                            > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                        result = true;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }

            public void onSwipeRight() {
                if(config.getSwipeDirRight()) {
                    updatePage(next_page);
                } else {
                    updatePage(previous_page);
                }
            }
            public void onSwipeLeft() {
                if(config.getSwipeDirRight()) {
                    updatePage(previous_page);
                } else {
                    updatePage(next_page);
                }
            }
            public void onSwipeTop() {
            }
            public void onSwipeBottom() {
            }
        });
        photoView.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent motionEvent) {
                if(config.getDoubleTap()) {
                    updatePage(next_page);
                }
                return false;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent motionEvent) {
                return false;
            }
        });

        refreshPages();

        imageList = new PhotoView[CONSTANT.AMOUNT_PAGES];

        new ImageLoaderThread().start();

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        System.out.println("\n\n\nLets see if we have some files: " + CONSTANT.AMOUNT_PAGES
        + " | " + imageList.length);

        if(config.getSwipeNavigation()) {
            updatePage(default_page);
        } else {
            loadImagesForImageView();
        }
    }

    private void updatePage(int pageSkim) {
        switch (pageHandler.checkPagesBoundary(pageSkim)) {
            case INSIDE:
                CONSTANT.CURRENT_PAGE_SHOWN += pageSkim;
                updateImageView();
                break;
            case LOWER:
                //System.out.println("IS OUTSIDE BOUNDARY!");
                updateChapter(-1);
                break;
            case HIGHER:
                //System.out.println("IS OUTSIDE BOUNDARY!");
                updateChapter(1);
                break;
        }
    }

    private void updateChapter(int chapterSkim) {
        switch (chapterHandler.checkChaptersBoundary(chapterSkim)) {
            case INSIDE:
                //RESET Page
                CONSTANT.CURRENT_PAGE_SHOWN = 0;
                CONSTANT.CURRENT_CHAPTER_SHOWN += chapterSkim;
                CONSTANT.CURRENT_CHAPTER_DIR = chapterHandler.getChapter(
                        CONSTANT.CURRENT_CHAPTER_SHOWN);

                refreshPages();
                new ImageLoaderThread().start();

                //We need the little pause to load the first PhotoView
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                updateImageView();
                //Displaying Toast with the Chaptername message
                Toast.makeText(binding.photoView.getContext(),
                        chapterHandler.getChapterName(),Toast.LENGTH_SHORT).show();
                break;
            case LOWER:
                //Displaying Toast with the first Chapter message
                Toast.makeText(binding.photoView.getContext(),
                        "First Chapter",Toast.LENGTH_SHORT).show();
                break;
            case HIGHER:
                //Displaying Toast with the last Chapter message
                Toast.makeText(binding.photoView.getContext(),
                        "Last Chapter",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void updateImageView() {
        int currentPage = CONSTANT.CURRENT_PAGE_SHOWN;
        if (imageList != null && imageList.length > 0 && imageList[currentPage] != null) {
            layout.removeView(photoView);
            photoView = imageList[currentPage];

            layout.addView(photoView);
        } else {
            //Get the information to show next image
            String imagePath = files[CONSTANT.CURRENT_PAGE_SHOWN].getAbsolutePath();
            photoView.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }

        textNumber.setText((CONSTANT.CURRENT_PAGE_SHOWN + 1) + "/" + CONSTANT.AMOUNT_PAGES);

        //to quickly comeback to read
        backupToQuickStartup();
    }

    private void refreshPages() {
        //Getting the current directory path
        String path = CONSTANT.CURRENT_CHAPTER_DIR;
        File directory = new File(path);
        files = directory.listFiles();
        Arrays.sort(files);
        CONSTANT.AMOUNT_PAGES = files.length;

        System.out.println(checkStoragePermissions());
        ActivityResultContracts.RequestPermission permission = new ActivityResultContracts.RequestPermission();
        permission.createIntent(getContext(),MediaStore.VOLUME_EXTERNAL);
        System.out.println("permission= " + permission);

        System.out.println("path= " + path);
        System.out.println("how many files are in: " + directory.getPath() + " amount: " + directory.listFiles().length);

    }

    public boolean checkStoragePermissions(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            //Android is 11 (R) or above
            return Environment.isExternalStorageManager();
        }else {
            //Below android 11
            int write = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int read = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);

            return read == PackageManager.PERMISSION_GRANTED && write == PackageManager.PERMISSION_GRANTED;
        }
    }

    /*
    This is Method is for Scroll Navigation
     */
    private void loadImagesForImageView() {
        linearLayoutForScrollNav.setOrientation(LinearLayout.VERTICAL);

        layout.removeView(photoView);
        layout.removeView(textNumber);

        new ImageLoaderThread().start();

        layout.addView(scrollViewForScrollNav);
        scrollViewForScrollNav.addView(linearLayoutForScrollNav);
    }

    private void backupToQuickStartup(){
        //Write them into the File
        SharedPreferences sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_folder), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(getString(R.string.save_folder), CONSTANT.CURRENT_CHAPTER_DIR);
        editor.apply();

        sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_chapter_number), Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        editor.putInt(getString(R.string.save_chapter_number), CONSTANT.CURRENT_CHAPTER_SHOWN);
        editor.apply();

        sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_page_number), Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        editor.putInt(getString(R.string.save_page_number), CONSTANT.CURRENT_PAGE_SHOWN);
        editor.apply();
    }

    private void callDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Page Selector");

        // Add the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });


        Dialog dialog = builder.create();
            dialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private class ImageLoaderThread extends Thread {

        private void update() {
            imageList = new PhotoView[CONSTANT.AMOUNT_PAGES];

            // first we load next Pages
            // and after that we load previous Pages
            for(int i = CONSTANT.CURRENT_PAGE_SHOWN; i < CONSTANT.AMOUNT_PAGES; i++) {
                if(imageList[i] == null) {
                    loadImages(i);
                    System.out.println("LOADED PAGE: " + i);
                }
            }
            for(int i = CONSTANT.CURRENT_PAGE_SHOWN - 1; i >= 0; i--) {
                if(imageList[i] == null) {
                    loadImages(i);
                    System.out.println("LOADED PAGE: " + i);
                }
            }
        }

        private void loadImages(int pageNumber) {
            String imagePath = files[pageNumber].getAbsolutePath();
            PhotoView image = new PhotoView(getContext());

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT
            );

            image.setLayoutParams(params);

            image.setImageBitmap(BitmapFactory.decodeFile(imagePath));

            image.setOnSingleFlingListener(new OnSingleFlingListener() {
                private static final int SWIPE_THRESHOLD = 100;
                private static final int SWIPE_VELOCITY_THRESHOLD = 100;

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    boolean result = false;
                    try {
                        float diffY = e2.getY() - e1.getY();
                        float diffX = e2.getX() - e1.getX();
                        if (Math.abs(diffX) > Math.abs(diffY)) {
                            if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX)
                                    > SWIPE_VELOCITY_THRESHOLD) {
                                if (diffX > 0) {
                                    onSwipeRight();
                                } else {
                                    onSwipeLeft();
                                }
                                result = true;
                            }
                        }
                        else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY)
                                > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffY > 0) {
                                onSwipeBottom();
                            } else {
                                onSwipeTop();
                            }
                            result = true;
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    return result;
                }

                public void onSwipeRight() {
                    if(config.getSwipeDirRight()) {
                        updatePage(next_page);
                    } else {
                        updatePage(previous_page);
                    }
                }
                public void onSwipeLeft() {
                    if(config.getSwipeDirRight()) {
                        updatePage(previous_page);
                    } else {
                        updatePage(next_page);
                    }
                }
                public void onSwipeTop() {
                }
                public void onSwipeBottom() {
                }
            });
            image.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
                @Override
                public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
                    return false;
                }

                @Override
                public boolean onDoubleTap(MotionEvent motionEvent) {
                    if(config.getDoubleTap()) {
                        updatePage(next_page);
                    }
                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent motionEvent) {
                    return false;
                }
            });

            imageList[pageNumber] = image;
        }

        public void continuousImageLoad() {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT
            );

            for(int i = 0; i <CONSTANT.AMOUNT_PAGES; i++) {
                PhotoView pv = imageList[i];
                pv.setLayoutParams(params);

                linearLayoutForScrollNav.addView(pv);
            }
        }

        @Override
        public void run() {
            Looper.prepare();
            update();

            if(config.getSwipeNavigation() == false) {
                continuousImageLoad();
            }
        }
    }
}
