package ch.reader.controller;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.io.File;
import java.util.Arrays;

import ch.reader.R;
import ch.reader.constant.CONSTANT;
import ch.reader.databinding.FragmentFolderviewBinding;

public class TitleViewFragment extends Fragment {

    //Bindings and stuff
    private FragmentFolderviewBinding binding;
    private LinearLayout linearLayoutDirList;

    private ActivityResultLauncher<Intent> storageActivityResultLauncher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    new ActivityResultCallback<ActivityResult>(){
            @Override
            public void onActivityResult(ActivityResult o) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                    //Android is 11 (R) or above
                    if(Environment.isExternalStorageManager()){
                        //Manage External Storage Permissions Granted
                        Log.d(TAG, "onActivityResult: Manage External Storage Permissions Granted");
                    }else{
                        Toast.makeText(getContext(), "Storage Permissions Denied", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    //Below android 11
                }
            }
        });

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        binding = FragmentFolderviewBinding.inflate(inflater, container, false);
        linearLayoutDirList = binding.linearLayoutDirList;

        if (!checkStoragePermissions()) {
            System.out.println("requestForStoragePermissions()");
            startActivity(new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION));
        } else {
            System.out.println("Already have permission");
        }

        CONSTANT.CURRENT_TITLE_DIR      = CONSTANT.DEFAULT_DIR;
        CONSTANT.CURRENT_CHAPTER_DIR    = CONSTANT.DEFAULT_DIR;

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String path     = CONSTANT.DEFAULT_DIR;
        File directory  = new File(path);
        File[] files    = directory.listFiles();

        System.out.println("how many files are in: " + directory.getPath() + " amount: " + directory.listFiles().length);

        for(File file : directory.listFiles()) {
            System.out.println(file.getName());
        }

        Arrays.sort(files);

        for(int i = 0; i < files.length; i++) {
            File file = files[i];
            if(file.isDirectory())
                linearLayoutDirList.addView(addButton(files[i].getName()));
        }
    }

    private Button addButton(String buttonName) {
        Button titleButton = new Button(linearLayoutDirList.getContext());
        titleButton.setText(buttonName);
        titleButton.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);

        titleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CONSTANT.CURRENT_TITLE_DIR = (CONSTANT.DEFAULT_DIR + "/" + buttonName);

                //Navigate to the next View
                NavHostFragment.findNavController(TitleViewFragment.this)
                        .navigate(R.id.action_ChapterViewFragment_to_ChapterViewFragment);
            }
        });

        return titleButton;
    }

    public boolean checkStoragePermissions(){
        System.out.println("checking");
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            System.out.println("Android is 11 (R) or above");
            //Android is 11 (R) or above
            System.out.println(Environment.isExternalStorageManager());
            return Environment.isExternalStorageManager();
        } else {
            //Below android 11
            int write = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int read = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);

            System.out.println("Below android 11");
            return read == PackageManager.PERMISSION_GRANTED && write == PackageManager.PERMISSION_GRANTED;

        }
    }

    private void requestForStoragePermissions() {
        //Android is 11 (R) or above
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            try {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package",  getActivity().getPackageName(), null);
                intent.setData(uri);
                storageActivityResultLauncher.launch(intent);
            }catch (Exception e){
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                storageActivityResultLauncher.launch(intent);
            }
        }else{
            //Below android 11
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    },
                    23
            );
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
