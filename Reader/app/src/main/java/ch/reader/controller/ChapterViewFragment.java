package ch.reader.controller;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.io.File;
import java.util.Arrays;

import ch.reader.R;
import ch.reader.constant.CONSTANT;
import ch.reader.controller.handler.ChapterHandler;
import ch.reader.databinding.FragmentChapterviewBinding;

public class ChapterViewFragment extends Fragment {

    //Bindings and stuff
    private FragmentChapterviewBinding binding;
    private LinearLayout linear_layout_chapterDir;

    private ChapterHandler chapterHandler = ChapterHandler.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentChapterviewBinding.inflate(inflater, container, false);
        linear_layout_chapterDir = binding.linearLayoutChapterDir;

        //RESET Chapter-path if the user returns to the page.
        CONSTANT.CURRENT_CHAPTER_DIR = CONSTANT.CURRENT_TITLE_DIR;
        //RESET Chapter-list
        chapterHandler.clearCHAPTERLIST();

        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Getting the current directory path
        chapterHandler.initializeChapters();

        //Set Fragment Title
        System.out.println(CONSTANT.getTitleName());
//        getActivity().getActionBar();
//        getActivity().getActionBar().setTitle(CONSTANT.getTitleName());
//        getParentFragment().getActivity().getActionBar().setTitle(CONSTANT.getTitleName());
//        getActivity().setTitle(CONSTANT.getTitleName());

        int id = 0;
        for(String chapterName : chapterHandler.getChapterNameList()) {
            linear_layout_chapterDir.addView(addButton(id, chapterName));
            id++;
        }
    }

    //Create Buttons for Subfolders
    private Button addButton(int id, String buttonName) {
        Button chapterButton = new Button(linear_layout_chapterDir.getContext());
        chapterButton.setId(id);
        chapterButton.setText(buttonName);
        chapterButton.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);

        chapterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SET parameters
                CONSTANT.CURRENT_CHAPTER_DIR    = CONSTANT.CURRENT_TITLE_DIR + "/" + buttonName;
                CONSTANT.CURRENT_PAGE_SHOWN     = 0;
                CONSTANT.CURRENT_CHAPTER_SHOWN  = id;

                //Navigate to the next View
                NavHostFragment.findNavController(ChapterViewFragment.this)
                        .navigate(R.id.action_show_images);
            }
        });

        return chapterButton;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
