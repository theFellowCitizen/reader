package ch.reader.controller;

import static android.Manifest.permission.MANAGE_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;

import ch.reader.R;
import ch.reader.constant.CONSTANT;
import ch.reader.constant.Config;
import ch.reader.controller.handler.ChapterHandler;
import ch.reader.databinding.FragmentStartpageviewBinding;

public class StartPageFragment extends Fragment {
    //Bindings and stuff
    private FragmentStartpageviewBinding binding;
    private LinearLayout linearLayout;
    private Button titleButton;
    private Button settingsButton;
    private FloatingActionButton skipButton;

    private Config config = Config.getInstance();
    private ChapterHandler chapterHandler = ChapterHandler.getInstance();

    /**
     * Returns the root folders of all the available external storages.
     */
        File getAppSpecificAlbumStorageDir(Context context, String albumName) {
            // Get the pictures directory that's inside the app-specific directory on
            // external storage.
            File file = new File(context.getExternalFilesDir(
                    Environment.DIRECTORY_PICTURES), albumName);
            if (file == null || !file.mkdirs()) {
//                Log.e(LOG_TAG, "Directory not created");
                System.out.println("directory not created");
            }
            return file;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding         = FragmentStartpageviewBinding.inflate(inflater, container, false);

        linearLayout    = binding.linearLayout;
        titleButton     = new Button(linearLayout.getContext());
        settingsButton  = new Button(linearLayout.getContext());
        skipButton      = binding.skipButton;

        return binding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //create Buttons and add them to the View
        linearLayout.addView(titleButton);
        linearLayout.addView(settingsButton);
        setUpButtons();

        //user Configs
        loadUserConfig();
//        config.readLastBackup();
    }

    private void setUpButtons() {
        titleButton.setText("Titles");
        settingsButton.setText("Settings");

        titleButton.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        settingsButton.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);

        titleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SET parameters
                //Navigate to the next View
                NavHostFragment.findNavController(StartPageFragment.this)
                        .navigate(R.id.action_to_FolderViewFragment);
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SET parameters
                //Navigate to the next View
                NavHostFragment.findNavController(StartPageFragment.this)
                        .navigate(R.id.action_to_settings_fragment);
            }
        });

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(linearLayout.getContext(),
                        "Quickskip to Titles", Toast.LENGTH_SHORT).show();
                //SET parameters
                //Navigate to the next View
                NavHostFragment.findNavController(StartPageFragment.this)
                        .navigate(R.id.action_StartPageViewFragment_to_ImageViewFragment);
            }
        });

        SharedPreferences sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_folder), Context.MODE_PRIVATE);
        String folderPath =  sharedPref.getString(
                getString(R.string.save_folder), null);

        sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_chapter_number), Context.MODE_PRIVATE);
        int chapterNumber = sharedPref.getInt(
                getString(R.string.save_chapter_number), 0);

        sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_page_number), Context.MODE_PRIVATE);
        int pageNumber = sharedPref.getInt(
                getString(R.string.save_page_number), 0);

        if(folderPath != null) {
            String[] split = folderPath.split("/");
            String title = split[split.length - 2];
            String chapter = split[split.length - 1];
            Button quickStart = new Button(linearLayout.getContext());
            quickStart.setText(title + " " + chapter );
            quickStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CONSTANT.CURRENT_TITLE_DIR      = CONSTANT.DEFAULT_DIR + "/" + title;
                    CONSTANT.CURRENT_CHAPTER_DIR    = folderPath;
                    CONSTANT.CURRENT_CHAPTER_SHOWN  = chapterNumber;
                    CONSTANT.CURRENT_PAGE_SHOWN     = pageNumber;
                    System.out.println(title +" "+chapter + " " + chapterNumber);
                    chapterHandler.initializeChapters();

                    //Navigate to the next View
                    NavHostFragment.findNavController(StartPageFragment.this)
                            .navigate(R.id.action_StartPageViewFragment_to_ImageViewFragment);
                }
            });

            linearLayout.addView(quickStart);
        }
    }


    private void loadUserConfig() {
        //Load User Configuration
        config.readConfig(getContext());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
