package ch.reader.controller.handler;

import android.content.Context;
import android.media.MediaScannerConnection;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import ch.reader.constant.CONSTANT;

public class ChapterHandler {
    public static ChapterHandler chapterHandlerInstance = new ChapterHandler();
    private int amountChapters = 0;
    private ArrayList<String> chapterNameList = new ArrayList<>();
    private ArrayList<String> chapterDirPathList = new ArrayList<>();

    /* A private Constructor prevents any other
     * class from instantiating. AKA Singleton
     */
    private ChapterHandler() { }

    /* Static 'instance' method */
    public static ChapterHandler getInstance( ) {
        if(chapterHandlerInstance == null) {
            chapterHandlerInstance = new ChapterHandler();
        }
        return chapterHandlerInstance;
    }

    public BoundaryCheck checkChaptersBoundary(int skipAmount) {
        BoundaryCheck isInBoundary = BoundaryCheck.INSIDE;
        int progress = CONSTANT.CURRENT_CHAPTER_SHOWN + skipAmount;

        if(progress < 0)
            isInBoundary = BoundaryCheck.LOWER;
        else if(progress == CONSTANT.AMOUNT_CHAPTERS)
            isInBoundary = BoundaryCheck.HIGHER;

        return isInBoundary;
    }

    public String getChapterName() {
        return chapterNameList.get(CONSTANT.CURRENT_CHAPTER_SHOWN);
    }

    public void addCHAPTER(String name) {
        String chapterDirPath = CONSTANT.CURRENT_TITLE_DIR + "/" + name;

        chapterNameList.add(name);
        chapterDirPathList.add(chapterDirPath);
    }

    public String getChapter(int id) {
        return chapterDirPathList.get(id);
    }

    public int amountChapters() {
        return chapterDirPathList.size();
    }

    public void clearCHAPTERLIST() {
        chapterDirPathList.clear();
        chapterNameList.clear();
    }

    public ArrayList<String> getChapterNameList() {
        return chapterNameList;
    }

    public void initializeChapters() {
        clearCHAPTERLIST();

        String path= CONSTANT.CURRENT_TITLE_DIR;
        File directory = new File(path);
        File[] files   = directory.listFiles();
        Arrays.sort(files);

        //Iterate through all the Subfolders and add Buttons
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            if(file.isDirectory()) {
                String chapterName = files[i].getName();
                //POPULATE the ArrayList and ADD the Button to the View
                addCHAPTER(chapterName);
            }
        }
        //SET amount Chapters
        CONSTANT.AMOUNT_CHAPTERS = amountChapters();
    }
}
