package ch.reader.controller.handler;

import ch.reader.constant.CONSTANT;

public class PageHandler {
    public static PageHandler pageHandlerInstance = new PageHandler();

    /* A private Constructor prevents any other
     * class from instantiating. AKA Singleton
     */
    private PageHandler() { }

    /* Static 'instance' method */
    public static PageHandler getInstance( ) {
        if(pageHandlerInstance == null) {
            pageHandlerInstance = new PageHandler();
        }
        return pageHandlerInstance;
    }

    public BoundaryCheck checkPagesBoundary(int skipAmount) {
        BoundaryCheck isInBoundary = BoundaryCheck.INSIDE;

        int progress = CONSTANT.CURRENT_PAGE_SHOWN + skipAmount;

        if(progress < 0) {
            isInBoundary = BoundaryCheck.LOWER;
        }
        else if(progress >= CONSTANT.AMOUNT_PAGES) {
            isInBoundary = BoundaryCheck.HIGHER;
        }

        return isInBoundary;
    }
}
