package ch.reader.controller;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import ch.reader.R;
import ch.reader.constant.Config;
import ch.reader.databinding.FragmentSettingsBinding;

public class SettingsFragment extends Fragment {
    //Bindings and stuff
    private FragmentSettingsBinding binding;
    private Config config = Config.getInstance();

    //Buttons
    private Button buttonSave;
    private Button buttonCancel;
    private Button buttonpermissiion;

    //double Tap
    private CheckBox checkDoubleTap;

    // RadioButtons Swipe Direction
    private RadioGroup swipeDirection;
    private RadioButton swipeLeftToRight;
    private RadioButton swipeRightToLeft;

    // RadioButtons Swipe Direction
    private RadioGroup swipeNavigationRadio;
    private RadioButton swipeNavigation;
    private RadioButton scrollNavigation;

    private ActivityResultLauncher<Intent> storageActivityResultLauncher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    new ActivityResultCallback<ActivityResult>(){

                        @Override
                        public void onActivityResult(ActivityResult o) {
                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                                //Android is 11 (R) or above
                                if(Environment.isExternalStorageManager()){
                                    //Manage External Storage Permissions Granted
                                    Log.d(TAG, "onActivityResult: Manage External Storage Permissions Granted");
                                }else{
                                    Toast.makeText(getContext(), "Storage Permissions Denied", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                //Below android 11

                            }
                        }
                    });

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSettingsBinding.inflate(inflater, container, false);

        //Buttons
        buttonSave = binding.buttonSave;
        buttonCancel = binding.buttonCancel;
        buttonpermissiion = binding.buttonPermission;

        checkDoubleTap = binding.checkDoubleTap;

        // RadioButtons SwipeDirection
        swipeNavigationRadio = binding.swipeNavigationRadio;
        swipeNavigation = binding.swipeRadio;
        scrollNavigation = binding.scrollRadio;

        // RadioButtons SwipeDirection
        swipeDirection = binding.swipeDirection;
        swipeLeftToRight = binding.swipeLeftToRight;
        swipeRightToLeft = binding.swipeRightToLeft;

        buttonpermissiion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkStoragePermissions() == false) {
                    requestForStoragePermissions();
                } else {
                    System.out.println("Already have permission");
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });

        return binding.getRoot();
    }

    public boolean checkStoragePermissions(){
        System.out.println("checking");
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            System.out.println("Android is 11 (R) or above");
            //Android is 11 (R) or above
            System.out.println(Environment.isExternalStorageManager());
            return Environment.isExternalStorageManager();
        }else {
            //Below android 11
            int write = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int read = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);

            System.out.println("Below android 11");
            return read == PackageManager.PERMISSION_GRANTED && write == PackageManager.PERMISSION_GRANTED;
        }
    }

    private void requestForStoragePermissions() {
        //Android is 11 (R) or above
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            try {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package",  getActivity().getPackageName(), null);
                intent.setData(uri);
                storageActivityResultLauncher.launch(intent);
            }catch (Exception e){
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                storageActivityResultLauncher.launch(intent);
            }
        }else{
            //Below android 11
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    },
                    23
            );
        }

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //set doubleTap CheckBox
        checkDoubleTap.setChecked(config.getDoubleTap());

        //set Swipe Scroll Navigation RadioButton
        if(config.getSwipeNavigation()) {
            swipeNavigation.toggle();
        } else {
            scrollNavigation.toggle();
        }


        //set SwipeDir RadioButton
        if(config.getSwipeDirRight()) {
            swipeLeftToRight.toggle();
        } else {
            swipeRightToLeft.toggle();
        }

    }

    private void save() {
        boolean isDoubleTap = checkDoubleTap.isChecked();

        boolean isSwipeDirRight = true;
        boolean isSwipNavigation = true;

        // Read the Settings
        // Read Swipe Direction
        int radioButtonID = swipeDirection.getCheckedRadioButtonId();
        RadioButton button = swipeDirection.findViewById(radioButtonID);
        if(button == swipeLeftToRight) {
            System.out.println("Right Swipe");
            isSwipeDirRight = true;
        } else {
            System.out.println("Left Swipe");
            isSwipeDirRight = false;
        }

        // Read Swipe Scroll Navigation
        radioButtonID = swipeNavigationRadio.getCheckedRadioButtonId();
        button = swipeNavigationRadio.findViewById(radioButtonID);
        if(button == swipeNavigation) {
            System.out.println("Swipe Mode enabled");
            isSwipNavigation = true;
        } else {
            System.out.println("Scroll Mode enabled");
            isSwipNavigation = false;
        }

        config.setDoubleTab(isDoubleTap);
        config.setSwipeDirection(isSwipeDirRight);
        config.setSwipeNavigation(isSwipNavigation);

        // Write them into the File
        // SAVE DoubleTap
        SharedPreferences sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_double_tap), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putBoolean(getString(R.string.save_double_tap), isDoubleTap);
        editor.apply();

        // SAVE Swipe Scroll Navigation
        sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_swipeNavigation), Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        editor.putBoolean(getString(R.string.save_swipeNavigation), isSwipNavigation);
        editor.apply();

        // SAVE SwipeDirection
        sharedPref = getContext().getSharedPreferences(
                getString(R.string.save_swipedirection), Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        editor.putBoolean(getString(R.string.save_swipedirection), isSwipeDirRight);
        editor.apply();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
