package ch.reader.controller.handler;

public enum BoundaryCheck {
    INSIDE,
    HIGHER,
    LOWER;
}
