package ch.reader.constant;

import android.os.Environment;

import java.io.File;
import java.util.ArrayList;

public class CONSTANT {
    public static final String DEFAULT_DIR = Environment.getExternalStorageDirectory().toString()
            + "/QuickManga";
    public static String CURRENT_TITLE_DIR = DEFAULT_DIR;
    public static String CURRENT_CHAPTER_DIR = CURRENT_TITLE_DIR;

    //Boundary for Images in Chapter
    public static int AMOUNT_PAGES;
    public static int CURRENT_PAGE_SHOWN = 0;

    //Boundary for Chapters in Folder
    public static int AMOUNT_CHAPTERS;
    public static int CURRENT_CHAPTER_SHOWN = 0;

//    public static String LAST_VISITED_FOLDER    = null;
//    public static String LAST_VISITED_PAGE_PATH = null;
//    public static int LAST_VISITED_PAGE         = 0;

    public static String getTitleName() {
        String title = null;

        if (CONSTANT.CURRENT_TITLE_DIR != null && !CONSTANT.CURRENT_TITLE_DIR.equals("")) {
           String[] titleDir = CONSTANT.CURRENT_TITLE_DIR.split("/");

           if (titleDir.length > 0) {
               System.out.println("\nCurrent_Title_Dir: " + CONSTANT.CURRENT_TITLE_DIR);
               title = titleDir[titleDir.length - 1];
           }
        }

        return title;
    }
}
