package ch.reader.constant;

import android.content.Context;
import android.content.SharedPreferences;

import ch.reader.R;

public class Config {
    private static Config instance = null;

    //Configurations
    private boolean isDoubleTab;
    private boolean isSwipeNavigation;
    private boolean isSwipeDirRight;

    private Config() { }

    public static Config getInstance() {
        if(instance == null) {
            instance = new Config();
        }
        return instance;
    }

    public void setDoubleTab(boolean doubleTab) {
        this.isDoubleTab = doubleTab;
    }

    public boolean getDoubleTap() {
        return this.isDoubleTab;
    }

    public void setSwipeDirection(boolean isSwipeDirRight) {
        this.isSwipeDirRight = isSwipeDirRight;
    }

    public boolean getSwipeDirRight() {
        return this.isSwipeDirRight;
    }

    public void setSwipeNavigation(boolean isSwipeNavigation) {
        this.isSwipeNavigation = isSwipeNavigation;
    }

    public boolean getSwipeNavigation()  {
        return isSwipeNavigation;
    }

    public void readConfig(Context context) {

        //load double Tap first
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.save_double_tap), Context.MODE_PRIVATE);
        this.isDoubleTab = sharedPref.getBoolean(
                context.getString(R.string.save_double_tap), false);

        // Load Swipe Scroll Navigation
        sharedPref = context.getSharedPreferences(
                context.getString(R.string.save_swipeNavigation), Context.MODE_PRIVATE);
        this.isSwipeNavigation = sharedPref.getBoolean(
                context.getString(R.string.save_swipeNavigation), true);

        // Load Swipe Direction
        sharedPref = context.getSharedPreferences(
                context.getString(R.string.save_swipedirection), Context.MODE_PRIVATE);
        this.isSwipeDirRight = sharedPref.getBoolean(
                context.getString(R.string.save_swipedirection), false);
    }
}
